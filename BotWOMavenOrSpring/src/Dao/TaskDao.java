package Dao;

import Entities.Task;
import Helpers.Config;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TaskDao {

    private Connection connection = Config.getDbConn();

    public boolean addTask(Task task){
        try {
            PreparedStatement statement = connection.prepareStatement("insert into tasks (\"task\", \"creatorid\", " +
                    "\"done\") values (?, ?, ?)");
            statement.setString(1, task.getTask());
            statement.setLong(2, task.getUsersId());
            statement.setBoolean(3, false);
            statement.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public Task findTask (Task task) {
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM tasks WHERE \"task\" = ? AND \"creatorid\" = ?");
            statement.setString(1, task.getTask());
            statement.setLong(2, task.getUsersId());
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                return new Task(
                        rs.getLong("creatorid"),
                        rs.getString("task"),
                        rs.getBoolean("done"));
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Task> findTasksByUserId(long userId) {
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM tasks WHERE \"creatorid\" = ?");
            statement.setLong(1, userId);
            ResultSet rs = statement.executeQuery();
            List<Task> tasks = new ArrayList<>();
            while (rs.next()) {
                Task task = new Task(
                        rs.getLong("creatorid"),
                        rs.getString("task"),
                        rs.getBoolean("done"));
                tasks.add(task);
            }
            if (tasks.isEmpty()) return null;
            return tasks;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean deleteTask (Task task){
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM tasks WHERE \"task\"= ? " +
                    "AND \"creatorid\" = ?");
            statement.setString(1, task.getTask());
            statement.setLong(2, task.getUsersId());
            statement.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean isThereATask (Task task){
        return findTask(task) != null;
    }

    public boolean setDoneTask (Task task, boolean done){
        try {
            PreparedStatement statement = connection.prepareStatement("UPDATE tasks SET \"done\" = ? " +
                    "WHERE \"task\" = ? AND \"creatorid\" = ?");
            statement.setBoolean(1, done);
            statement.setString(2, task.getTask());
            statement.setLong(3, task.getUsersId());
            statement.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
}
