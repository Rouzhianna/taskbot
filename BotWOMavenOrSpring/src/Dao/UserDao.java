package Dao;
import Entities.User;
import Helpers.Config;
import Helpers.Hash;

import java.sql.*;

public class UserDao {

    private Connection connection = Config.getDbConn();

    public boolean addUser(User user){
        if (findUserById(user.getId()) != null){
            return false;
        }
        try {
            PreparedStatement statement = connection.prepareStatement("insert into users (\"id\", \"doption\") values (?,?)");
            statement.setLong(1, user.getId());
            statement.setInt(2, user.getdOption());
            statement.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public User findUserById(long id) {
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM users WHERE \"id\" = ?");
            statement.setLong(1, id);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                return new User(
                        rs.getLong("id"),
                        rs.getInt("doption"));
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean userExists (long id){
        return findUserById(id) != null;
    }

    public int getDOption (long id) {
        User user = findUserById(id);
        return (user == null? 0 : user.getdOption());
    }

    public boolean setDOption (long user, int option) {
        try {
            PreparedStatement statement = connection.prepareStatement("UPDATE users SET \"doption\" = ? WHERE \"id\"=?");
            statement.setInt(1, option);
            statement.setLong(2, user);
            statement.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    public boolean setPwd (long user, String pwd){
        pwd = Hash.getMd5Hash(pwd);
        try {
            PreparedStatement statement = connection.prepareStatement("UPDATE users" +
                    " SET \"password\" = ? WHERE \"id\"=?");
            statement.setString(1, pwd);
            statement.setLong(2, user);
            statement.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }
}
