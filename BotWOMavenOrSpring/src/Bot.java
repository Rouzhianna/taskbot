import Dao.TaskDao;
import Dao.UserDao;
import Entities.Task;
import Entities.User;
import Helpers.Config;
import Helpers.Utilities;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.util.List;

public class Bot extends TelegramLongPollingBot {

    private final String botToken = "430863011:AAEYaX01vK8FqwJe4J5sX-Y8fsoRHIWYEg4";
    private final String botUsername = "n33na_bot";

    private TaskDao taskDao = new TaskDao();
    private UserDao userDao = new UserDao();

    @Override
    public void onUpdateReceived(Update update) {
        if(update.hasMessage()){
            Message message = update.getMessage();
            long chatId = message.getChatId();
            int dOption = userDao.getDOption(chatId);
            boolean flag;
            if (message.hasText()) {
                String s;
                if (message != null && message.hasText()) {
                    if (message.getText().charAt(0) == '/') {
                        switch (message.getText().toLowerCase()) {
                            case "/start":
                                flag = userDao.userExists(chatId);
                                if (flag) {
                                    s = "Ты уже зарегистрирован. Используй команду /create для создания занятий.";
                                    userDao.setDOption(chatId, 0);
                                } else {
                                    flag = userDao.addUser(new User(chatId, 5));
                                    s = (flag ? "Ура! Ты теперь зарегистрирован! Теперь можешь использовать " +
                                            "команду /create для создания дел. Для входа через сайт можешь ввести " +
                                            "свой новый пароль сейчас, или по команде /pwd."
                                            : "Произошла ошибка при регистрации, попробуй позже.");
                                    System.out.println(chatId);
                                }
                                break;
                            case "/pwd":
                                userDao.setDOption(chatId, 5);
                                s = "Введи новый пароль.";
                                break;
                            case "/user":
                                userDao.setDOption(chatId, 0);
                                s = String.valueOf(userDao.findUserById(chatId).getId());
                                break;
                            case "/tasks":
                                flag = sendTasks(message, taskDao.findTasksByUserId(chatId));
                                s = "У тебя еще нет дел! Используй /create для их создания!";
                                System.out.println(chatId);
                                userDao.setDOption(chatId, 0);
                                if (flag) {
                                    return;
                                }
                                break;
                            case "/help":
                                userDao.setDOption(chatId, 0);
                                s = "Регистрация: /start \n" +
                                        "Создать занятие: /create \n" +
                                        "Вывод всех занятий: /tasks \n" +
                                        "Удалить дело: /delete \n" +
                                        "Отметить дело сделанным: /done \n" +
                                        "Отметить дело несделанным: /undone \n" +
                                        "Сменить пароль: /pwd \n" +
                                        "Узнать свой логин : /user";
                                break;
                            case "/create":
                                if (!userDao.userExists(chatId))
                                    s = "Ты не можешь создавать дела, пока не зарегистрируешься. " +
                                            "Введи /start для начала работы со мной.";
                                else {
                                    userDao.setDOption(chatId, 10);
                                    s = "Введи название занятия, которое ты хочешь начать.";
                                }
                                break;
                            case "/delete":
                                s = "Введи номер дела для удаления. Чтобы узнать его, используй команду /tasks";
                                userDao.setDOption(chatId, 20);
                                break;
                            case "/done":
                                userDao.setDOption(chatId, 30);
                                s = "Введи номер дела, которое ты сделал. Чтобы узнать его, используй команду /tasks";
                                break;
                            case "/undone":
                                userDao.setDOption(chatId, 40);
                                s = "Введи номер дела для повторного выполнения. Чтобы узнать его, используй команду /tasks";
                                break;
                            case "/cancel":
                                userDao.setDOption(chatId, 0);
                                s = "Понял-принял, что хочешь сделать?";
                                break;
                            default:
                                s = "Чо-то я не понивилль.";
                        }
                    }else {
                        String text = message.getText();
                        boolean success;
                        List<Task> list;
                        int number;
                        switch (dOption) {
                            case 5:
                                if (text.length() > 15 || text.length() < 3 ){
                                    s = "Пароль должен быть от 3 до 15 знаков!";
                                }else {
                                    userDao.setPwd(chatId, text);
                                    userDao.setDOption(chatId, 0);
                                    s = "Пароль успешно установлен.";
                                }
                                break;
                            case 10:
                                Task task = new Task(chatId, text);
                                if (!taskDao.isThereATask(task)){
                                    if(taskDao.addTask(task))
                                        s = "Дело успешно создано.";
                                    else s = "Произошла ошибка, свяжись с @pe5ka, либо попробуй попозже.";
                                    userDao.setDOption(chatId, 0);
                                }
                                else {
                                    if (taskDao.findTask(task).isDone()) {
                                        s = "Дело уже выполнено. Вы можете отметить его его как несделанное с " +
                                                "помощью команды /undone, или выбрать другое название.";
                                    }else
                                        s = "Дело с таким названием уже существует. Выберите другое название.";
                                }
                                break;
                            case 20:
                                if (Utilities.isNumeric(text)){
                                    number = Integer.parseInt(text);
                                    list = taskDao.findTasksByUserId(chatId);
                                    if (number > list.size()){
                                        s = "Число слишком большое, попробуй еще раз.";
                                    }else if (number < 1) {
                                        s = "Число меньше нуля, попробуй еще раз.";
                                    }else {

                                        success = taskDao.deleteTask(list.get(number-1));
                                        s = (success ? "Дело успешно удалено." : "Просизошла ошибка при удалении.");
                                        userDao.setDOption(chatId, 0);
                                    }
                                }else
                                    s = "Необходимо ввести номер дела.";
                                break;
                            case 30:
                                if (Utilities.isNumeric(text)){
                                    number = Integer.parseInt(text);
                                    list = taskDao.findTasksByUserId(chatId);
                                    if (number > list.size()){
                                        s = "Число слишком большое, попробуй еще раз.";
                                    }else if (number < 1) {
                                        s = "Число меньше нуля, попробуй еще раз.";
                                    }else {
                                        success = taskDao.setDoneTask(taskDao.findTasksByUserId(chatId).get(Integer.parseInt(text)-1), true);
                                        if (success){
                                            s = "Дело успешно выполнено. Хотите удалить? Используйте /delete";
                                        }else
                                            s = "Просизошла ошибка.";
                                        userDao.setDOption(chatId, 0);
                                    }
                                }else
                                    s = "Необходимо ввести номер дела.";
                                break;
                            case 40:
                                if (Utilities.isNumeric(text)){
                                    number = Integer.parseInt(text);
                                    list = taskDao.findTasksByUserId(chatId);
                                    if (number > list.size()) {
                                        s = "Число слишком большое, попробуй еще раз.";
                                    }else if (number < 1) {
                                        s = "Число меньше нуля, попробуй еще раз.";
                                    }else {
                                        success = taskDao.setDoneTask(taskDao.findTasksByUserId(chatId).get(Integer.parseInt(text) - 1), false);
                                        if (success) {
                                            s = "Дело успешно начато заново.";
                                        } else s = "Просизошла ошибка.";
                                        userDao.setDOption(chatId, 0);
                                    }
                                }else
                                    s = "Необходимо ввести номер дела.";
                                break;
                            default:
                                s = "Чо-то я не понивилль.";
                                userDao.setDOption(chatId, 0);
                                break;
                        }
                    }
                    sendMsg(message, s);
                }
            }
        }
    }

    public boolean sendTasks(Message message, List<Task> list){
        String s = "";
        try {
            for (int i = 0; i < list.size(); i++){
                Task task = list.get(i);
                s += (i + 1) + ": " + task.getTask() + " - ";
                s += ((task.isDone()? "Сделано" : "Не сделано") + "\n");
            }
            sendMsg(message, s);
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    private void sendMsg (Message message, String s){
        SendMessage sendMessage = new SendMessage(message.getChatId().toString(), s);
        sendMessage.enableMarkdown(true);
        sendMessage.setReplyToMessageId(message.getMessageId());
        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getBotUsername() {
        return Config.BOT_USERNAME;
    }

    @Override
    public String getBotToken() {
        return Config.BOT_TOKEN;
    }

}
