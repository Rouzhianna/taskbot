package Helpers;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class Config {

    public static final String CONFIGURATION_BOT_FILE = "./config/bot/bot.properties";
    public static final String CONFIGURATION_DB_FILE = "./config/database/database.properties";

    public static String BOT_USERNAME;
    public static String BOT_TOKEN;

    public static String DB_URL;
    public static String DB_USER;
    public static String DB_PWD;


    private static final String DRIVER = "org.postgresql.Driver";

    private static Connection dbConn = null;

    public static Connection getDbConn() {
        if(dbConn == null){
            try {
                Class.forName(DRIVER);
                dbConn = DriverManager.getConnection(Config.DB_URL, Config.DB_USER, Config.DB_PWD);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return dbConn;
    }

    public static void load(){
        Properties botSettings = new Properties();
        try(InputStream is = new FileInputStream(new File(CONFIGURATION_BOT_FILE))){
            botSettings.load(is);
            is.close();
            System.out.println("Конфигурация бота загружена успешно.");
        }catch (Exception e){
            System.out.println("!!! Ошибка загрузки конфигурации бота.");
        }

        Properties databaseSettings = new Properties();
        try (InputStream is = new FileInputStream(new File(CONFIGURATION_DB_FILE))){
            databaseSettings.load(is);
            is.close();
            System.out.println("Конфигурация БД загружена успешно.");
        }catch (Exception e){
            System.out.println("!!!Ошибка загрузки конфигурации БД.");
        }

        BOT_USERNAME = botSettings.getProperty("BotUserName", "n33na_bot");
        BOT_TOKEN = botSettings.getProperty("BotToken", "430863011:AAEYaX01vK8FqwJe4J5sX-Y8fsoRHIWYEg4");

        DB_URL = databaseSettings.getProperty("DbURL", "jdbc:postgresql://localhost:5432/BotDB");
        DB_USER = databaseSettings.getProperty("DbUser", "postgres");
        DB_PWD = databaseSettings.getProperty("DbPwd", "321");

    }


}
