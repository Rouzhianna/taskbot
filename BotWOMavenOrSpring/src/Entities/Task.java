package Entities;

public class Task {

    private long usersId;
    private String task;
    private boolean done;

    public Task (long usersId, String task, boolean done){
        this.usersId = usersId;
        this.task = task;
        this.done = done;
    }


    public Task(long usersId, String task) {
        this.usersId = usersId;
        this.task = task;
        this.done = false;
    }

    public long getUsersId() {
        return usersId;
    }

    public void setUsersId(long usersId) {
        this.usersId = usersId;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }
}
