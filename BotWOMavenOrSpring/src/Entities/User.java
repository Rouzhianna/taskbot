package Entities;

public class User {

    private long id;
    private int dOption;
    private String pwd;

    public int getdOption() {
        return dOption;
    }

    public void setdOption(int dOption) {
        this.dOption = dOption;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User(long id, int dOption) {
        this.id = id;
        pwd = "";
        this.dOption = dOption;
    }

    public User (long id) {
        this.id = id;
        pwd = "";
    }

    public User(long id, int dOption, String pwd) {
        this.id = id;
        this.dOption = dOption;
        this.pwd = pwd;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }
}
