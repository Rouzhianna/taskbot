package com.ru.nina.springbotweb.Springbotweb.service.impl;

import com.ru.nina.springbotweb.Springbotweb.repository.UserRepository;
import com.ru.nina.springbotweb.Springbotweb.security.detail.UserDetailsImpl;
import com.ru.nina.springbotweb.Springbotweb.service.AuthService;
import com.ru.nina.springbotweb.Springbotweb.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

@Service
public class AuthServiceImpl implements AuthService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User getUserByAuthentication(Authentication authentication) {
        System.out.println("AuthService is getting your authentication");
        UserDetailsImpl currentUserDetails = (UserDetailsImpl) authentication.getPrincipal();
        User currentUser = currentUserDetails.getUser();

        return userRepository.findUserById(currentUser.getId()).get();
    }
}
