package com.ru.nina.springbotweb.Springbotweb.controller;

import com.ru.nina.springbotweb.Springbotweb.dto.UserDto;
import com.ru.nina.springbotweb.Springbotweb.model.User;
import com.ru.nina.springbotweb.Springbotweb.service.impl.UserServiceImpl;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class AuthController {

    /*@RequestMapping(value = "/login")
    @PreAuthorize("isAnonymous()")
    public String login(@ModelAttribute("userDto") UserDto userDto, BindingResult result, ModelMap map ){
        System.out.println(userDto != null);
        User user = new UserServiceImpl().findUser(userDto.getChatId());
        User user2 = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(user2 != null && user.getId() == userDto.getChatId()){
            System.out.println("redirecting from login");
            return "redirect:/";
        }
        System.out.println("login: your auth is null");
        return "login";
    }*/

    @GetMapping("/login")
    public String login (Authentication authentication) {
        System.out.println("I'm in AuthController, method login.");
        if (authentication != null){
            System.out.println("You're logged, going to main page.");
            return "redirect:/profile";
        }
        System.out.println("You're not logged in, showing login form.");
        return "login";
    }

    /*@PostMapping(value = {"/login"})
    public String loginAttempt(Authentication authentication){
        authentication = new AuthProvider().authenticate(authentication);
        System.out.println(authentication == null);
        if(authentication != null){
            System.out.println("redirecting from loginattempt");
            return "profile";
        }
        System.out.println("attempt: your auth is null");
        return "login";
    }*/

    @GetMapping("/logout")
    public String logout(HttpServletRequest request, Authentication authentication){
        if(authentication != null){
            request.getSession().invalidate();
        }
        return "redirect:/login";
    }

}
