package com.ru.nina.springbotweb.Springbotweb.service;

import com.ru.nina.springbotweb.Springbotweb.model.Task;
import com.ru.nina.springbotweb.Springbotweb.model.User;

import java.util.List;

public interface TaskService {

    void save(Task task);

    Task findTaskByUserAndTask(User user, String text);

    //Task findTaskById(Long id);

    List<Task> findTasksByUser(User user);
}
