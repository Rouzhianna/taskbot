package com.ru.nina.springbotweb.Springbotweb.security.provider;

import com.ru.nina.springbotweb.Springbotweb.model.User;
import com.ru.nina.springbotweb.Springbotweb.repository.UserRepository;
import com.ru.nina.springbotweb.Springbotweb.security.detail.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

@Component
public class AuthProvider implements AuthenticationProvider {

    @Autowired
    private UserDetailsServiceImpl userDetailsService;
    @Autowired
    private UserRepository userRepository;


    @Override
    @PreAuthorize("isAnonymous()")
    public Authentication authenticate( Authentication authentication) throws AuthenticationException {
        System.out.println("now in authprovider, " + authentication.getName());
        String login = authentication.getName();
        Long longlogin = Long.parseLong(login);
        System.out.println("trying to find you..");
        String password = authentication.getCredentials().toString();
        System.out.println("your password is " + password);
        Optional<User> userOptional = userRepository.findUserById(longlogin);
        boolean flag = userOptional.isPresent();
        System.out.println(flag + " that optional's present");
        if(flag){
            User user = userOptional.get();
            System.out.println(user.getId() + " with pas " + password + "is inside if flag");
            System.out.println(Hash.getMd5Hash(password) + " vs. " + password);
            if(Hash.getMd5Hash(password).equals(user.getPassword())){
                Collection<? extends GrantedAuthority> authorities = Collections.singleton(new SimpleGrantedAuthority("ROLE_USER"));
                System.out.println("AuthProvider is saving " + login + " now");
                userRepository.save(user);
                UserDetails userDetails = userDetailsService.loadUserById(longlogin);
                return new UsernamePasswordAuthenticationToken(userDetails, password, authorities);
            }else {
                System.out.println("wr pas");
                throw new BadCredentialsException("Wrong password.");
            }
        } else {
            System.out.println("wr lo");
            throw new BadCredentialsException("Wrong id.");
        }
    }



    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }
}
