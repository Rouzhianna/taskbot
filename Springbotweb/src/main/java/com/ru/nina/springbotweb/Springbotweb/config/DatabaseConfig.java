package com.ru.nina.springbotweb.Springbotweb.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@ComponentScan("com.ru.nina.springbotweb.Springbotweb")
@Configuration
@PropertySource("classpath:db.properties")
@EnableJpaRepositories(basePackages = "com.ru.nina.springbotweb.Springbotweb.repository")
@EnableTransactionManagement
public class DatabaseConfig {

    @Autowired
    private Environment environment;

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();

        dataSource.setDriverClassName(this.environment.getProperty("database.driver"));
        dataSource.setUrl(this.environment.getProperty("database.url"));
        dataSource.setUsername(this.environment.getProperty("database.username"));
        dataSource.setPassword(this.environment.getProperty("database.password"));

        return dataSource;
    }

}