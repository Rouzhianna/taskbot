package com.ru.nina.springbotweb.Springbotweb.controller;

import com.ru.nina.springbotweb.Springbotweb.dto.TaskDto;
import com.ru.nina.springbotweb.Springbotweb.service.AuthService;
import com.ru.nina.springbotweb.Springbotweb.service.TaskService;
import com.ru.nina.springbotweb.Springbotweb.model.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class TaskController {

    /*@Autowired
    private AuthService authService;

    @Autowired
    private TaskService taskService;

    @GetMapping("/task/new")
    public String getNewTaskPage(Model model, TaskDto taskDto) {
        model.addAttribute("task", taskDto);

        return "create-hike";
    }

    @PostMapping("/task/new")
    public String saveTask(@ModelAttribute("task") @Valid TaskDto taskDto, Authentication authentication) {
        Task task = new Task();

        task.setTask(taskDto.getText());
        task.setUser(authService.getUserByAuthentication(authentication));
        taskService.save(task);

        return "redirect:/task/" + task.getTask() + "+" + task.getUser().getId();
    }

    *//*@GetMapping("task/{id}")
    public String showTask(Model model, @PathVariable("id") Long id){
        model.addAttribute("task", taskService.fin(id));
        return "task";
    }*//*

    @GetMapping("/tasks")
    public String showTasks (Model model, @ModelAttribute("task") @Valid TaskDto taskDto, Authentication authentication) {
        List<Task> tasks = taskService.findTasksByUser(authService.getUserByAuthentication(authentication));
        model.addAttribute("tasks", tasks);
        return "profile";
    }*/
}
