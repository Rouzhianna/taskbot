package com.ru.nina.springbotweb.Springbotweb.service.impl;

import com.ru.nina.springbotweb.Springbotweb.repository.UserRepository;
import com.ru.nina.springbotweb.Springbotweb.service.UserService;
import com.ru.nina.springbotweb.Springbotweb.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public User findUser(Long chatId) {
        Optional<User> o = userRepository.findUserById(chatId);
        User finding;
        if (o.isPresent()){
            finding = o.get();
            return finding;
        }else
            System.out.println("didn't found ya");
        return null;

    }
}
