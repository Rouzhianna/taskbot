package com.ru.nina.springbotweb.Springbotweb.repository;

import com.ru.nina.springbotweb.Springbotweb.model.Task;
import com.ru.nina.springbotweb.Springbotweb.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TaskRepository extends JpaRepository<Task, Long> {

    Task findTaskByUserAndTask(User user, String task);
    //Task findTaskByCreatoridAndTask(Long creatorid, String Task);
    //Task findTaskById(Long id);

    List<Task> findTasksByUser(User user);

}
