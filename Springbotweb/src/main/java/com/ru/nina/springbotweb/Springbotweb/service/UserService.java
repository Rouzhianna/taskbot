package com.ru.nina.springbotweb.Springbotweb.service;

import com.ru.nina.springbotweb.Springbotweb.model.User;
public interface UserService {
    User findUser(Long chatId);
}
