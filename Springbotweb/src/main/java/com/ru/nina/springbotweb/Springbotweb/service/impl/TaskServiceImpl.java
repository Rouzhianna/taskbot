package com.ru.nina.springbotweb.Springbotweb.service.impl;

import com.ru.nina.springbotweb.Springbotweb.repository.TaskRepository;
import com.ru.nina.springbotweb.Springbotweb.service.TaskService;
import com.ru.nina.springbotweb.Springbotweb.model.Task;
import com.ru.nina.springbotweb.Springbotweb.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Override
    public void save(Task task) {
        taskRepository.save(task);
    }

    @Override
    public Task findTaskByUserAndTask(User user, String text) {
        return taskRepository.findTaskByUserAndTask(user, text);
    }

    /*@Override
    public Task findTaskById(Long id) {
        return taskRepository.findTaskById(id);
    }
*/
    @Override
    public List<Task> findTasksByUser(User user) {
        return taskRepository.findTasksByUser(user);
    }
}
