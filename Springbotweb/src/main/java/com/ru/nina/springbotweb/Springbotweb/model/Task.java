package com.ru.nina.springbotweb.Springbotweb.model;

import javax.persistence.*;

@Entity
@Table(name = "tasks")
public class Task {

    @Column(name = "done")
    private boolean done;

    @Column(name = "task")
    private String task;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "creatorid", referencedColumnName = "id")
    private User user;

    public Task() {
        done = false;
    }

    public Task(boolean done, String task, User user) {
        this.done = done;
        this.task = task;
        this.user = user;
    }

    public Task(boolean done, String task, Long creatorId) {
        this.done = done;
        this.task = task;
        this.user.setId(creatorId);
    }

    public Task(String task, User user) {
        done = false;
        this.task = task;
        this.user = user;
    }

    public Task (String task, Long creatorId) {
        this.task = task;
        user.setId(creatorId);
    }

    public boolean getDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
