package com.ru.nina.springbotweb.Springbotweb.security.detail;

import com.ru.nina.springbotweb.Springbotweb.repository.UserRepository;
import com.ru.nina.springbotweb.Springbotweb.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserRepository userRepository;

    @Autowired
    public UserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        return loadUserById(Long.parseLong(login));
    }

    public UserDetails loadUserById(Long chatId) {
        Optional<User> ouser = userRepository.findUserById(chatId);
        if(ouser.isPresent()){
            System.out.println("returning new userDetailsImpl for " + ouser.get().getId());
            return new UserDetailsImpl(ouser.get());
        }
        return null;
    }
}
