package com.ru.nina.springbotweb.Springbotweb.service;

import com.ru.nina.springbotweb.Springbotweb.model.User;
import org.springframework.security.core.Authentication;

public interface AuthService {
    User getUserByAuthentication(Authentication authentication);
}
