package com.ru.nina.springbotweb.Springbotweb.controller;

import com.ru.nina.springbotweb.Springbotweb.dto.TaskDto;
import com.ru.nina.springbotweb.Springbotweb.model.Task;
import com.ru.nina.springbotweb.Springbotweb.model.User;
import com.ru.nina.springbotweb.Springbotweb.service.AuthService;
import com.ru.nina.springbotweb.Springbotweb.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class ProfileController {

    @Autowired
    private AuthService authService;
    @Autowired
    private TaskService taskService;

    @GetMapping("/")
    public String root(Authentication authentication){
        if (authentication != null){
            return "redirect:/profile";
        }else {
            return "redirect:/login";
        }
    }

    @GetMapping("/profile")
    public String getProfilePage (Authentication authentication, Model model){
        User user = authService.getUserByAuthentication(authentication);
        model.addAttribute("user", user);
        model.addAttribute("taskForm", new TaskDto());
        List<Task> tasks = taskService.findTasksByUser(user);
        model.addAttribute("aretheretasks", !tasks.isEmpty());
        model.addAttribute("tasks", tasks);
        return "profile";
    }
}
