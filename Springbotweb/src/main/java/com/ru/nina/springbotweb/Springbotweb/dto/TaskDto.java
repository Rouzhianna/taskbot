package com.ru.nina.springbotweb.Springbotweb.dto;

import com.ru.nina.springbotweb.Springbotweb.model.User;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class TaskDto {

    @NotNull
    @NotEmpty
    private User user;

    @NotNull
    @NotEmpty
    private Long creatorid;

    @NotNull
    @NotEmpty
    private String text;

    public Long getCreatorid() {
        return creatorid;
    }

    public void setCreatorid(Long creatorid) {
        this.creatorid = creatorid;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getText() { return text; }

    public void setText(String text) { this.text = text; }
}
