package com.ru.nina.springbotweb.Springbotweb.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "users")
public class User {

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "password")
    private String password;

    @Column(name = "doption")
    private int dOption;

    @OneToMany (mappedBy = "user", fetch = FetchType.EAGER)
    private List<Task> tasks;

    public int getdOption() {
        return dOption;
    }

    public void setdOption(int dOption) {
        this.dOption = dOption;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public int getDOption() {
        return dOption;
    }

    public void setDOption(int doption) {
        this.dOption = doption;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User(Long id, String password, int dOption) {
        this.id = id;
        this.password = password;
        this.dOption = dOption;
    }

    public User() {}

    public User(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", password='" + password + '\'' +
                ", doption='" + dOption + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return (id.equals(user.getId()) && password.equals(user.getPassword()));
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + password.hashCode();
        return result;
    }
}
