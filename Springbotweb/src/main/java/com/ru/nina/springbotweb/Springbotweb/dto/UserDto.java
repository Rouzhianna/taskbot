package com.ru.nina.springbotweb.Springbotweb.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserDto {

    private Long chatId;

    @NotNull
    @NotEmpty
    @Size(min = 3)
    private String password;

    private int dOption;


    public Long getChatId() {
        return chatId;
    }

    public void setChatId(Long chatId) {
        this.chatId = chatId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getdOption() {
        return dOption;
    }

    public void setdOption(int dOption) {
        this.dOption = dOption;
    }
}
